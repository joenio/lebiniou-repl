// LEBINIOU websocket API client

const WebSocket = require('ws')
const readline = require('readline');

// this script is based on lebiniou/www/static/lebiniou.js
// by lebiniou authors

// // COMMANDS CMD reference:
// //   * lebiniou/COMMANDS.md

let lb_ws = null;
try {
  const lb_ws_url = process.env.npm_package_config_url;
  console.log(`Connecting to Le Biniou ${lb_ws_url}...`);
  lb_ws = new WebSocket(lb_ws_url);
}
catch(e) {
  console.error('Error connecting to Le Biniou!');
  console.error(e);
  process.exit(1);
}

function command(cmd) {
  // console.log("SEND", ({ "command": cmd }));
  console.log(`lebiniou! ${cmd}`);
  lb_ws.send(JSON.stringify({ "command": cmd }));
}

function ui_command(cmd, arg) {
  // console.log("SEND", ({ "ui_command": cmd, "arg": arg }));
  console.log(`lebiniou! ${cmd} ${arg}`);
  lb_ws.send(JSON.stringify({ "ui_command": cmd, "arg": arg }));
}

function vui_command(cmd, arg) {
  console.log(`lebiniou!! vui commands not implemented yet`);
}

lb_ws.onopen = function() {
  console.log("Connected!");
  ui_command('UI_CMD_CONNECT', null);
  ui_command('UI_CMD_APP_GET_SHORTCUTS', 'COL');
  ui_command('UI_CMD_APP_GET_SHORTCUTS', 'IMG');
  //sai e fecha lebiniou
  //command("CMD_APP_QUIT")
  //command("CMD_APP_CLEAR_SCREEN")
  //troca cursor entre capturado ou nao pela janela do lebiniou
  //command("CMD_APP_SWITCH_CURSOR")
};

// TODO: create a payload stream back on hrung to have mix the JSON data with visuals
lb_ws.onmessage = function(event) {
  var payload = JSON.parse(event.data);
  //console.info(payload);
};

lb_ws.onerror = function(err) {
  console.error('Socket encountered error: ', err.message, 'Closing socket');
  lb_ws.close();
};

lb_ws.onclose = () => {
  console.log('Close!');
};

// REPL client command line cli interface

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
  prompt: 'lebiniou> ',
});

rl.prompt();
rl.on('line', (input) => {
    const cmd = input.trim().split(' ')[0];
    const arg = input.trim().split(' ')[1];
    switch (cmd) {
      case '3d+':
      case 'n3d':
      case 'next3d':
      case 'next-3d':
        // Next 3D boundary
        command("CMD_APP_NEXT_3D_BOUNDARY")
        break
      case '3d':
      case 't3d':
      case 'toggle3d':
      case 'toggle-3d':
        // Toggle 3D rotations
        command("CMD_APP_TOGGLE_3D_ROTATIONS")
        break
      case 'cam+':
        //Select next webcam
        command("CMD_APP_NEXT_WEBCAM")
        break
      case 'noauto':
      case 'autooff':
        //Turn off all auto changes
        command("CMD_APP_STOP_AUTO_MODES")
        break;
      case 'color':
      case 'colormap':
        //Display current colormap
        command("CMD_APP_DISPLAY_COLORMAP")
        break;
      case 'color-':
      case 'prevcolormap':
      case 'prev-colormap':
      case 'previouscolormap':
      case 'previous-colormap':
        //Select previous colormap
        command("CMD_COL_PREVIOUS")
        break
      case 'color+':
      case 'nextcolormap':
      case 'next-colormap':
        //Select next colormap
        command("CMD_COL_NEXT")
        break
      case 'color?':
      case 'rand-colormap':
      case 'randcolormap':
      case 'randomcolormap':
      case 'random-colormap':
        //Select random colormap
        command("CMD_COL_RANDOM")
        break
      case 'pixels':
        //Fill current frame with random pixels
        command("CMD_APP_RANDOMIZE_SCREEN")
        break
      case 'clear':
        //Clear the current frame
        command("CMD_APP_CLEAR_SCREEN")
        break
      case 'img-':
        //Select previous image
        command("CMD_IMG_PREVIOUS")
        break
      case 'img+':
        //Select next image
        command("CMD_IMG_NEXT")
        break
      case 'img?':
        //Select random image
        command("CMD_IMG_RANDOM")
        break
      case 'seq?':
      case 'randsequence':
      case 'rand-sequence':
      case 'randomsequence':
      case 'random-sequence':
        //Make a sequence from system schemes
        command("CMD_APP_RANDOM_SCHEME")
        break
      case 'seq-':
        //Use previous sequence
        command("CMD_APP_PREVIOUS_SEQUENCE")
        break
      case 'seq+':
        //Use next sequence
        command("CMD_APP_NEXT_SEQUENCE")
        break
      case 'vol+':
      case 'volup':
      case 'vol-up':
      case 'volumeup':
      case 'volume-up':
        //Scale volume up
        command("CMD_APP_VOLUME_SCALE_UP")
        break
      case 'vol-':
      case 'voldown':
      case 'vol-down':
      case 'volumedown':
      case 'volume-down':
        //Scale volume down
        command("CMD_APP_VOLUME_SCALE_DOWN")
        break
      case '3d++':
      case 'inc3d':
      case 'inc-3d':
      case 'increase3d':
      case 'increase-3d':
        //Increase 3D zoom
        command("CMD_APP_DEC_3D_SCALE")
        break
      case '3d--':
      case 'dec3d':
      case 'dec-3d':
      case 'decrease3d':
      case 'decrease-3d':
        //Decrease 3D zoom
        command("CMD_APP_INC_3D_SCALE")
        break
      case 'nextrandom':
      case 'next-random':
        //Next random mode
        command("CMD_APP_NEXT_RANDOM_MODE")
        break
      //to discover available plugins run
      //ls $(lebiniou.sh --help | grep 'Set base directory' | sed 's/.\+Set base directory \[//' | sed 's/\]//')/main/
      case 'plugin':
        ui_command('UI_CMD_APP_SELECT_PLUGIN', arg);
        command("CMD_APP_TOGGLE_SELECTED_PLUGIN")
        break
        //for more UI_* commands see:
        // https://gitlab.com/lebiniou/lebiniou-vui/-/blob/master/src/modules/uiCommand.ts
        // https://gitlab.com/lebiniou/lebiniou-vui/-/blob/master/src/views/MainView.vue
        // https://gitlab.com/lebiniou/lebiniou/-/blob/master/src/context_ui_commands.c
      //select command is here just as intermediate step to understand how the mechanism of selecting/activating logig works on lebiniou
      case 'select':
        ui_command('UI_CMD_APP_SELECT_PLUGIN', arg);
        break
      case 'up':
        //Move selected plugin up in the sequence
        command('CMD_SEQ_MOVE_UP');
        break
      case 'down':
        //Move selected plugin down in the sequence
        command('CMD_SEQ_MOVE_DOWN');
        break
      case 'reset':
        //Reset the current sequence
        command('CMD_SEQ_RESET');
        break
      case 'reorder': //non documented on COMMANDS.md
        //Reset the current sequence
        command('UI_CMD_SEQ_REORDER', ['image', 'gum_x']);
        break
      case 'save':
        //Save full sequence
        command('CMD_SEQ_SAVE_FULL')
        //TODO criar vui commands
        vui_command('VUI_RENAME_SEQUENCE', arg);
        break
      case 'quit':
      case 'close':
      case 'exit':
        rl.close()
        break
      default:
        console.log(`lebiniou? ${input.trim()}`)
    }
    rl.prompt()
  })
  .on('close', () => {
    console.log('\nlebiniou* bye-bye')
    lb_ws.close();
    process.exit(0)
  })
