# lebiniou-repl

A REPL text command-line interface to control Le Biniou.

**this prject is still in a alpha version**

## Installing

```sh
npm install
```

## How to use?

First you need to start Le Biniou (version >= 3.66.0) then run command below to
start `lebiniou-repl`.

```sh
npm start
```

It will connect on Le Biniou WebSocket "ws://127.0.0.1:30542/ui" and open a
prompt for commands that will be send through the WebSocket to Le Biniou.

```console
lebiniou> 
```

## Some examples of available commands

```console
lebiniou> next-3d
```

The command `next-3d` will be translated into Le Biniou command
`CMD_APP_NEXT_3D_BOUNDARY`.

The Le Biniou commands are documented on lebiniou source-code at:

* https://gitlab.com/lebiniou/lebiniou/-/blob/master/COMMANDS.md

```console
lebiniou> toggle-3d
```

The command `toggle-3d` will fire `CMD_APP_TOGGLE_3D_ROTATIONS` on Le Biniou.

To see all lebiniou-repl prompt commands see source-code on `main.js`, for now
as it is a very draft starting project version, there is no final documentation
yet, and until a first minimal stable version is done, reference documentarion
will be the source-code.

More examples:

```console
lebiniou> pixels
lebiniou> clear
lebiniou> color
lebiniou> color?
lebiniou> seq?
```

# Roadmap

The `lebiniou-repl` roadmap can be found at Hrung project.

- See https://codeberg.org/joenio/hrung/milestones

# Author

Joenio Marques da Costa <joenio@joenio.me>

# License

GPLv3
